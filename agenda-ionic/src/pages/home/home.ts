import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ContactPage } from '../contact/contact';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  items: string[] = ['Primer item', 'Segundo item', 'tercer item'];

  listaTareas: any[] = [];

  constructor(
    public navCtrl: NavController, 
    public httpClient: HttpClient) {

      this.loadTareas();

  }

  itemSelected() {
    this.navCtrl.push(ContactPage);
  }

  loadTareas() {
    this.httpClient.get('http://localhost:5000/api/tareas') //cambiar por ip
      .subscribe((response: any) => {
        console.log(response);
        this.listaTareas = response;
        // let titulos = response.map(x => x.titulo);
        this.items = response.map(x => x.titulo);
      });
  }
}
